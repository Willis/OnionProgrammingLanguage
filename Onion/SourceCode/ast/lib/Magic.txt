﻿/* tables */
 
table {
 
border-collapse: collapse;
 
text-align: left;
 
margin-bottom: 20px;
 
width:auto;
 
font-size: 12px;
 
color: #000000;
 
}
 
th.
empty
{
 
background: #fff;
 
}
 
tr.
empty
:hover {
 
background: #fff;
 
}
 
th {
 
background-color: #aaaaaa;
 
font-weight: normal;
 
font-size: 15px;
 
padding: 0 10px;
 
color: #fff;
 
text-shadow:#414141 0 1px 0px;
 
line-height: 40px;
 
}
 
tbody tr {
 
font-weight: normal;
 
background-color: #ffffff;
 
}
 
tbody tr:nth-child(even) {
 
background-color: #ffffff;
 
}
 
tbody tr:hover {
 
background-color: #eaeaea;
 
}
 
td {
 
padding: 9px 10px;
 
border-left: #d7d7d7 1px solid;
 
border-top: #d7d7d7 1px solid;
 
border-bottom: #d7d7d7 1px solid;
 
line-height: 20px;
 
}
 
tr td:first-child {
 
border-left:0;
 
}

html, body{ 
 margin:0; 
 padding:0; 
 } 
 
#pagewidth{ }
 
#maincol{
 background-color: #FFFFFF;  
 position: relative; 
 }

.clearfix:after {
 content: "."; 
 display: block; 
height: 0; 
 clear: both; 
 visibility: hidden;
 }
 
.clearfix{display: inline-block;}

/* Hides from IE-mac \*/
* html .clearfix{height: 1%;}
.clearfix{display: block;}
/* End hide from IE-mac */  